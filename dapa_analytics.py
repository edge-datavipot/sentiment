from twython import Twython
import re
from imports import *
from collections import defaultdict
from facepy import GraphAPI
import urllib2
from bs4 import BeautifulSoup

oauth_access_token='EAAUktZBXrf3cBAOOeWaeJ8XGixFaKTMJ8oH0FSPxx\
		f1LWZAT0NzdLBcKlq5U6iLKZBCYVmZBHY7Azg4aHvHCPZBR2psbPpkt9doxGsp\
		2TwzzPEpkve7v0MnBceLVzLyZCouNTH6uD0ZAutpMMqW8FMASqGqvqN9qy8ZD'
class dapaAnalytics(object):
	"""docstring for ClassName"""
	def __init__(self,twitterKey=None,twitterSecret=None,facebookKey=None,facebookSecret=None):
		self.facebookKey=facebookKey
		self.facebookSecret=facebookSecret
		self.twitterKey=twitterKey
		self.twitterSecret=twitterSecret
		#self.webPassKey=webPassKey
	def process_twitter(self,search,handle=None):
		hold_tweets=[]
		twitter = Twython(self.twitterKey,self.twitterSecret, oauth_version=2)
		ACCESS_TOKEN = twitter.obtain_access_token()
		twitter = Twython(self.twitterKey,access_token=ACCESS_TOKEN)
		
		if handle:			
			try:						
				user_timeline = twitter.get_user_timeline(screen_name=handle,include_retweets=False,count=200)
				for tweet in user_timeline:
					encoded_tweet=tweet['text'].encode('utf-8')
					hold_tweets.append(clean_message(encoded_tweet))				
			except:
				hold_tweets=[]

		if search:
				tweets=twitter.search(q=search,count=200)
				for result in tweets['statuses']:
					tweet_text = result['text'].encode('utf-8')
					hold_tweets.append(clean_message(tweet_text))

		return hold_tweets

	def process_facebook(self,pageId,limit=100,limit_comment=10):
		messages=[]
		ids=[]
		graph = GraphAPI(oauth_access_token)
		page_id =pageId
		endpoint="/feed"
		if pageId:	
			try:
				data = graph.get(str(page_id)+endpoint,page=False,retry=1,limit=limit,order='chronological')
				for msg in data['data']:    
					if msg.get('message'):						
						messages.append(clean_message(msg.get('message')))	
						ids.append(msg.get('id',None))	
				
				for comment_id in ids:
					getcomments=graph.get(comment_id+ "/comments",limit_comment)
					for coment in getcomments['data']:
						if coment.get('message'):
							print coment.get('message')							
							messages.append(clean_message(coment.get('message')))			
	
			except:
				pass

		return messages
	def process_web(self,webUrl):
		parsed=BeautifulSoup(urllib2.urlopen(webUrl))
		listOfTags=['title','h1','h2','h3','h4','h5','h6','p','section']
		messages=[]
		for tag in listOfTags:
			for text in parsed.findAll(tag):
				if text:
					messages.append(clean_message(text.text.replace("\n",'')))
		return messages


'''
#********** This is code for testing the above class which have been commented *****************

APP_KEY = 'LM8lWcCxieNGgUUvCgwAX3CR8' #supply the appropriate value
APP_SECRET = 'c2mOTcwMWGzH1vIkuBE7mZTJbSRoG3qqcihcjVRhbAcyl6yTno' 


testclass=dapaAnalytics(APP_KEY,APP_SECRET)
tweets=testclass.process_twitter('robertalai')
for i in tweets:
	print i

fb=dapaAnalytics(facebookKey=oauth_access_token)
fb_messages=fb.process_facebook(961332080629125)
#print fb.facebookKey
if(fb_messages):
	for i in fb_messages:
		print i
else:
	print 'Error'


testclass=dapaAnalytics()
messages=testclass.process_web('http://www.tukutane.co.ke')
for msg in messages:
	print msg
'''








