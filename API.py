from flask import Flask,jsonify
from collections import defaultdict
from dapa_analytics import *
from sklearn.externals import joblib
app=Flask(__name__)

APP_KEY =       'LM8lWcCxieNGgUUvCgwAX3CR8' 
APP_SECRET =   'c2mOTcwMWGzH1vIkuBE7mZTJbSRoG3qqcihcjVRhbAcyl6yTno' 
oauth_access_token='EAAUktZBXrf3cBAOOeWaeJ8XGixFaKTMJ8oH0FSPxx\
		f1LWZAT0NzdLBcKlq5U6iLKZBCYVmZBHY7Azg4aHvHCPZBR2psbPpkt9doxGsp\
		2TwzzPEpkve7v0MnBceLVzLyZCouNTH6uD0ZAutpMMqW8FMASqGqvqN9qy8ZD'

def return_json(tweets):
	pos=0
	neg=0
	loadvectorilizer=joblib.load("models/vectorizer.pkl")
	loadclassifier=joblib.load("models/edge.pkl")
	my_list=defaultdict(dict)
	if tweets:
		my_list['messages']=tweets	
		predictions=loadclassifier.predict(loadvectorilizer.transform(tweets))
		for predicted in predictions:
			if predicted==1:
				pos+=1				
			else:
				neg+=1		

		my_list['predictions']['positive']=pos
		my_list['predictions']['negative']=neg
	else:
		my_list['error']=1

	return jsonify(my_list)

@app.route("/twitter/<search>/")
@app.route("/twitter/<search>/<screenname>/")
def twitter(search,screenname=None):	
	fetch_tweets=dapaAnalytics(APP_KEY,APP_SECRET)
	tweets=fetch_tweets.process_twitter(search,screenname)
	return return_json(tweets)

@app.route("/facebook/<fbid>/")
def facebook(fbid):
	fetch_posts=dapaAnalytics(facebookKey=oauth_access_token)
	posts=fetch_posts.process_facebook(fbid)
	return return_json(posts)

if __name__ == '__main__':
	app.run(debug=True)

