from flask import Flask,jsonify
import json
from collections import defaultdict
app=Flask(__name__)
@app.route("/twitter/<screenname>/<search>/<stype>")
def api(screenname,search,stype):
	return screenname+search+stype
@app.route('/json')
def json():
	my_list=defaultdict(dict)
	#my_list['positive']=range(10)
	my_list['predictions']['pos']=range(10)
	my_list['predictions']['neg']=range(10)
	my_list['messages']['positive']=['msg1','msge2','msg3']
	my_list['messages']['negative']=['msg4','msg5','m']
	my_list['error']='None'	
	return jsonify(my_list)
if __name__ == '__main__':
	app.run(debug=True)

